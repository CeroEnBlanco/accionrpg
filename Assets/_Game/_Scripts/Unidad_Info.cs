﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TipoDeJugador { player1, player2, neutral }

public class Unidad_Info : MonoBehaviour
{
    public TipoDeJugador tipoDeJugador;
}
