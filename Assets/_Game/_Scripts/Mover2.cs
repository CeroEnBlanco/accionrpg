﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover2 : MonoBehaviour
{
    public Rigidbody rb;
    [SerializeField]
    Joystick joystick;
    [SerializeField]
    float velocidad;
    [SerializeField]
    Animator anim;
    [Space]
    public bool atacando;

    private void FixedUpdate()
    {
        if (Mathf.Abs(joystick.Horizontal) > .5f || Mathf.Abs(joystick.Vertical) > .5f)
        {
            if (!atacando)
            {
                Vector3 direccion = new Vector3(transform.position.x + joystick.Horizontal, transform.position.y, transform.position.z + joystick.Vertical) - transform.position;

                Quaternion newRotation = Quaternion.LookRotation(direccion);

                rb.MoveRotation(newRotation);

                transform.position = Vector3.MoveTowards(transform.position, transform.position + new Vector3(joystick.Horizontal, 0, joystick.Vertical), velocidad * Time.deltaTime);
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, transform.position + new Vector3(joystick.Horizontal, 0, joystick.Vertical), (velocidad / 2) * Time.deltaTime);
            }

            anim.SetBool("Run", true);
        }
        else
        {
            rb.velocity = Vector3.zero;

            anim.SetBool("Run", false);
        }
    }
}
