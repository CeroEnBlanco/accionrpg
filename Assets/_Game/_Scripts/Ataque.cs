﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

public class Ataque : MonoBehaviour
{
    [SerializeField]
    UnityEvent start_atk, end_atk;

    public void ActivarAtaque ()
    {
        start_atk.Invoke();
    }

    public void DesactivarAtaque ()
    {
        end_atk.Invoke();
    }
}
