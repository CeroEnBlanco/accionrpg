﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotar : MonoBehaviour
{
    [SerializeField]
    Joystick joystick;
    [SerializeField]
    Transform pov;
    [SerializeField]
    float vel_x, vel_y;

    private void Update()
    {
        if (Mathf.Abs(joystick.Horizontal) > .5f)
        {
            transform.Rotate((Vector3.up * joystick.Horizontal) * vel_x * Time.deltaTime);
        }

        if (Mathf.Abs(joystick.Vertical) > .5f)
        {
            if (pov.localRotation.eulerAngles.x >= 180f)
            {
                if (joystick.Vertical > 0 && pov.localRotation.eulerAngles.x > 300f)
                {
                    pov.Rotate((Vector3.right * -joystick.Vertical) * vel_y * Time.deltaTime);
                }
                else if(joystick.Vertical < 0)
                {
                    pov.Rotate((Vector3.right * -joystick.Vertical) * vel_y * Time.deltaTime);
                }
            }
            else if (pov.localRotation.eulerAngles.x < 180f)
            {
                if (joystick.Vertical > 0)
                {
                    pov.Rotate((Vector3.right * -joystick.Vertical) * vel_y * Time.deltaTime);
                }
                else if (joystick.Vertical < 0 && pov.localRotation.eulerAngles.x < 60f)
                {
                    pov.Rotate((Vector3.right * -joystick.Vertical) * vel_y * Time.deltaTime);
                }
            }
        }
    }
}
