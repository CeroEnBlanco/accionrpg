﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unidad_Ataque : MonoBehaviour
{
    [SerializeField]
    Enemigo_Movimiento em;
    [SerializeField]
    Animator anim;
    public Transform nexoEnemigo;
    public Unidad_Vida vida_objetivoActual;
    public List<Transform> enemigos = new List<Transform>();
    [SerializeField]
    int daño;
    public float distanciaDeAtk;
    [SerializeField]
    HitBox hitBox;

    private void Start()
    {
        if (GetComponent<Unidad_Info>())
        {
            if (GetComponent<Unidad_Info>().tipoDeJugador == TipoDeJugador.player1)
            {
                nexoEnemigo = GameObject.FindGameObjectWithTag("Nexo2").transform;
            }
            else if (GetComponent<Unidad_Info>().tipoDeJugador == TipoDeJugador.player2)
            {
                nexoEnemigo = GameObject.FindGameObjectWithTag("Nexo1").transform;
            }
        }
    }

    private void Update()
    {
        if (enemigos.Count > 0)
        {
            if (vida_objetivoActual.vidaActual <= 0)
            {
                RemoverObjetoDeLaListaDeEnemigos(enemigos[0]);

                anim.SetBool("Atacando", false);
            }
        }
        else
        {
            anim.SetBool("Atacando", false);
        }
    }

    public void RangoVision_Entrar(Transform nuevo)
    {
        if (GetComponent<Unidad_Info>().tipoDeJugador != nuevo.GetComponent<Unidad_Info>().tipoDeJugador && nuevo.GetComponent<Unidad_Info>().tipoDeJugador != TipoDeJugador.neutral)
        {
            if (!enemigos.Contains(nuevo))
            {
                enemigos.Add(nuevo);

                if (!vida_objetivoActual)
                {
                    vida_objetivoActual = enemigos[0].GetComponent<Unidad_Vida>();
                }
            }
        }
    }

    public void RangoVision_Salir(Transform nuevo)
    {
        RemoverObjetoDeLaListaDeEnemigos(nuevo);
    }

    public void Atacar()
    {
        hitBox.daño = daño;

        hitBox.gameObject.SetActive(true);
    }

    public void RemoverObjetoDeLaListaDeEnemigos (Transform objeto)
    {
        for (int i = 0; i < enemigos.Count; i++)
        {
            if (enemigos[i] == objeto)
            {
                enemigos.RemoveAt(i);

                if (vida_objetivoActual.transform == objeto)
                {
                    if (enemigos.Count > 0)
                    {
                        vida_objetivoActual = enemigos[0].GetComponent<Unidad_Vida>();
                    }
                    else
                    {
                        vida_objetivoActual = null;
                    }
                }

                break;
            }
        }
    }
}
