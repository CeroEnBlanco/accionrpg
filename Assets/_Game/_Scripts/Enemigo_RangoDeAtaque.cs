﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo_RangoDeAtaque : MonoBehaviour
{
    [SerializeField]
    Unidad_Ataque unidadAtaque;

    private void OnTriggerEnter(Collider other)
    {
        if (this.enabled && other.GetComponent<Unidad_Info>())
        {
            unidadAtaque.RangoVision_Entrar(other.transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (this.enabled && other.GetComponent<Unidad_Info>())
        {
            unidadAtaque.RangoVision_Salir(other.transform);
        }
    }
}
