﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atacar : MonoBehaviour
{
    [SerializeField]
    Mover2 mover;
    [SerializeField]
    Joystick joystick;
    [SerializeField]
    Animator anim;
    [SerializeField]
    GameObject prefab_proyectil;
    [SerializeField]
    float frecuencia;
    [Space]
    public Transform mira;
    [SerializeField]
    GameObject prefab_vfx;

    float t;

    private void Start()
    {
        t = frecuencia;
    }

    private void Update()
    {
        t += Time.deltaTime;

        if (Mathf.Abs(joystick.Horizontal) > .5f || Mathf.Abs(joystick.Vertical) > .5f)
        {
            mover.atacando = true;

            Vector3 direccion = new Vector3(transform.position.x + joystick.Horizontal, transform.position.y, transform.position.z + joystick.Vertical) - transform.position;

            Quaternion newRotation = Quaternion.LookRotation(direccion);

            mover.rb.MoveRotation(newRotation);

            if (t >= frecuencia)
            {
                //Disparar();

                t = 0;
            }
        }
        else
        {
            mover.atacando = false;
        }

        anim.SetBool("Atacando", mover.atacando);
    }

    public void Atk ()
    {


        GameObject new_go = (GameObject)Instantiate(prefab_vfx, mira.position, mira.rotation);

        Destroy(new_go, 1);
    }

    void Disparar ()
    {
        GameObject newProyectil = (GameObject)Instantiate(prefab_proyectil, mira.position, mira.rotation);
    }
}
