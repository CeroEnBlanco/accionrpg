﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    [SerializeField]
    Rigidbody rb;
    [SerializeField]
    Joystick joystick;
    [SerializeField]
    float velocidad;

    Vector3 movimiento;

    private void Update()
    {
        float h = 0, v = 0;

        if (joystick.Horizontal > .5f)
        {
            h = 1;
        }
        else if (joystick.Horizontal < -.5f)
        {
            h = -1;
        }

        if (joystick.Vertical > .5f)
        {
            v = 1;
        }
        else if (joystick.Vertical < -.5f)
        {
            v = -1;
        }

        movimiento = new Vector3(h, 0, v);

        if (Mathf.Abs(joystick.Horizontal) == 0 && Mathf.Abs(joystick.Vertical) == 0)
        {
            rb.velocity = Vector3.zero;

            rb.angularVelocity = Vector3.zero;
        }
    }

    private void FixedUpdate()
    {
        transform.Translate(movimiento.normalized * velocidad * Time.deltaTime);
    }
}
