﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invocador : MonoBehaviour
{
    [SerializeField]
    GameObject prefab;
    [SerializeField]
    float frecuencia;

    float t = 0;

    private void Update()
    {
        t += Time.deltaTime;

        if (t >= frecuencia)
        {
            GameObject newPrefab = (GameObject)Instantiate(prefab, transform.position, Quaternion.identity);

            t = 0;
        }
    }
}
