﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo_Vida : MonoBehaviour
{
    public int vidaInicial, vidaMax, vidaActual;

    private void Start()
    {
        vidaMax = vidaInicial;

        vidaActual = vidaMax;
    }

    public void RecibirDaño (int dañoEntrante)
    {
        if (dañoEntrante > 0)
        {
            vidaActual -= dañoEntrante;

            if (vidaActual < 0)
            {
                vidaActual = 0;
            }

            if (vidaActual == 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
