﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverRotar : MonoBehaviour
{
    [SerializeField]
    Rigidbody rb;
    [SerializeField]
    Joystick joystickDerecho, joystickIzquierdo;
    [SerializeField]
    float velocidad;
    [SerializeField]
    Transform pov;
    [SerializeField]
    float vel_x, vel_y;

    private void Update()
    {
        if (Mathf.Abs(joystickIzquierdo.Horizontal) > .5f)
        {
            transform.Rotate((Vector3.up * joystickIzquierdo.Horizontal) * vel_x * Time.deltaTime);
        }

        if (Mathf.Abs(joystickDerecho.Vertical) > .5f)
        {
            if (pov.localRotation.eulerAngles.x >= 180f)
            {
                if (joystickDerecho.Vertical > 0 && pov.localRotation.eulerAngles.x > 300f)
                {
                    pov.Rotate((Vector3.right * -joystickDerecho.Vertical) * vel_y * Time.deltaTime);
                }
                else if (joystickDerecho.Vertical < 0)
                {
                    pov.Rotate((Vector3.right * -joystickDerecho.Vertical) * vel_y * Time.deltaTime);
                }
            }
            else if (pov.localRotation.eulerAngles.x < 180f)
            {
                if (joystickDerecho.Vertical > 0)
                {
                    pov.Rotate((Vector3.right * -joystickDerecho.Vertical) * vel_y * Time.deltaTime);
                }
                else if (joystickDerecho.Vertical < 0 && pov.localRotation.eulerAngles.x < 60f)
                {
                    pov.Rotate((Vector3.right * -joystickDerecho.Vertical) * vel_y * Time.deltaTime);
                }
            }
        }

        if (Mathf.Abs(joystickIzquierdo.Horizontal) == 0 && Mathf.Abs(joystickIzquierdo.Vertical) == 0)
        {
            rb.velocity = Vector3.zero;

            rb.angularVelocity = Vector3.zero;
        }
    }

    private void FixedUpdate()
    {
        float h = 0, v = 0;

        if (Mathf.Abs(joystickDerecho.Horizontal) > .5f)
        {
            h = joystickDerecho.Horizontal;
        }

        if (Mathf.Abs(joystickIzquierdo.Vertical) > .5f)
        {
            v = joystickIzquierdo.Vertical;
        }

        transform.Translate(new Vector3(h, 0, v).normalized * velocidad * Time.deltaTime);
    }
}
