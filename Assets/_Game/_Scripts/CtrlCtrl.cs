﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class CtrlCtrl : MonoBehaviour
{
    [SerializeField]
    Mover mover;
    [SerializeField]
    Rotar rotar;
    [SerializeField]
    MoverRotar moverRotar;
    [SerializeField]
    bool encendido;
    [SerializeField]
    Image img;
    [SerializeField]
    Color color1, color2;

    private void Start()
    {
        encendido = false;

        Cambio();
    }

    public void Cambio ()
    {
        encendido = !encendido;

        if (encendido)
        {
            mover.enabled = false;
            rotar.enabled = false;

            moverRotar.enabled = true;

            img.color = color1;
        }
        else
        {
            moverRotar.enabled = false;

            mover.enabled = true;
            rotar.enabled = true;

            img.color = color2;
        }
    }
}
