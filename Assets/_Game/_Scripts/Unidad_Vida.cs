﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unidad_Vida : MonoBehaviour
{
    [SerializeField]
    CapsuleCollider collider;
    [SerializeField]
    Animator anim;
    public int vidaInicial, vidaMax, vidaActual;
    [Space]
    public GameObject hit_vfx;
    public Transform t_hit;

    private void Start()
    {
        vidaMax = vidaInicial;

        vidaActual = vidaMax;
    }

    public void RecibirDaño(int dañoEntrante, Transform causanteDelDaño)
    {
        if (vidaActual > 0 && dañoEntrante > 0)
        {
            vidaActual -= dañoEntrante;

            if (hit_vfx)
            {
                GameObject newVFX = (GameObject)Instantiate(hit_vfx, t_hit.position, t_hit.rotation, t_hit);

                Destroy(newVFX, 2);
            }

            if (vidaActual < 0)
            {
                vidaActual = 0;
            }

            if (vidaActual == 0)
            {
                if (collider)
                {
                    collider.enabled = false;
                }

                if (anim)
                {
                    anim.SetTrigger("Dead");
                }
            }
            else if (vidaActual > 0)
            {
                if (anim)
                {
                    anim.SetTrigger("Hurt");
                }
            }
        }
    }
}
