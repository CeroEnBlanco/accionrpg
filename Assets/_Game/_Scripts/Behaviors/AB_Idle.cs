﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AB_Idle : StateMachineBehaviour
{
    Enemigo_Movimiento em;
    Unidad_Ataque ua;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!em)
        {
            em = animator.GetComponentInParent<Enemigo_Movimiento>();
        }

        if (!ua)
        {
            ua = animator.GetComponentInParent<Unidad_Ataque>();
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (ua.enemigos.Count > 0)
        {
            if (em.navMesh.remainingDistance > ua.distanciaDeAtk)
            {
                em.Set_Destino(ua.enemigos[0].position);
            }
            else
            {
                animator.SetBool("Atacando", true);
            }
        }
        else
        {
            em.Set_Destino(em.posFinal);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
