﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AB_Atk : StateMachineBehaviour
{
    Enemigo_Movimiento em;
    Unidad_Ataque ua;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!em)
        {
            em = animator.GetComponentInParent<Enemigo_Movimiento>();
        }

        if (!ua)
        {
            ua = animator.GetComponentInParent<Unidad_Ataque>();
        }

        if (ua && ua.enemigos.Count > 0)
        {
            // Determine which direction to rotate towards
            Vector3 targetDirection = ua.enemigos[0].position - ua.transform.position;

            // The step size is equal to speed times frame time.
            float singleStep = 500f * Time.deltaTime;

            // Rotate the forward vector towards the target direction by one step
            Vector3 newDirection = Vector3.RotateTowards(ua.transform.forward, targetDirection, singleStep, 0.0f);

            // Draw a ray pointing at our target in
            //Debug.DrawRay(transform.position, newDirection, Color.red);

            // Calculate a rotation a step closer to the target and applies rotation to this object
            ua.transform.rotation = Quaternion.LookRotation(newDirection);
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //if (ua.enemigos.Count > 0)
        //{
        //    if (ua.vida_objetivoActual.vidaActual <= 0)
        //    {
        //        ua.RemoverObjetoDeLaListaDeEnemigos(ua.enemigos[0]);

        //        animator.SetBool("Atacando", false);
        //    }
        //}
        //else
        //{
        //    animator.SetBool("Atacando", false);
        //}
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("Atacando", false);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
