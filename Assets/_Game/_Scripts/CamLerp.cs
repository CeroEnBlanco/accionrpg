﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamLerp : MonoBehaviour
{
    [SerializeField]
    Transform target;
    [SerializeField]
    float distancia, velocidad;


    private void FixedUpdate()
    {
        if (target != null && Vector3.Distance(transform.position, target.position) > distancia)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, velocidad * Time.deltaTime);
        }
    }
}
