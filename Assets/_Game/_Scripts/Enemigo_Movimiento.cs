﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;

public class Enemigo_Movimiento : MonoBehaviour
{
    public NavMeshAgent navMesh;
    [SerializeField]
    Animator anim;
    [Space]
    public Vector3 posFinal;

    private void Start()
    {
        posFinal = GameObject.FindGameObjectWithTag("Nexo1").transform.position;

        Set_Destino(posFinal);
    }

    public void Set_Destino (Vector3 newDestino)
    {
        navMesh.SetDestination(newDestino);

        if (navMesh.remainingDistance > navMesh.stoppingDistance)
        {
            anim.SetBool("EnMovimiento", true);
        }
        else
        {
            anim.SetBool("EnMovimiento", false);
        }
    }
}
