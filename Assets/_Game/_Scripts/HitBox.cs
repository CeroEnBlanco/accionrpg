﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBox : MonoBehaviour
{
    public int daño;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.transform.name);

        if (GetComponentInParent<Unidad_Info>() && other.GetComponent<Unidad_Info>())
        {
            Unidad_Info info = GetComponentInParent<Unidad_Info>(), infoOtro = other.GetComponent<Unidad_Info>();

            if (info.tipoDeJugador != infoOtro.tipoDeJugador)
            {
                Debug.Log(info.gameObject.name + " ha impactado a " + infoOtro.gameObject.name);

                if (other.GetComponent<Unidad_Vida>())
                {
                    Unidad_Vida vida = other.GetComponent<Unidad_Vida>();

                    vida.RecibirDaño(daño, info.transform);
                }
            }
        }
    }
}
